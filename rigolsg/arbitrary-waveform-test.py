#!/usr/bin/env python2

import usbtmc
import time
from math import sin

listOfDevices = usbtmc.getDeviceList()
dn = listOfDevices[0]
d = usbtmc.UsbTmcDriver(dn)
#print d.getName()

def w(command, delay=0.2):
    print command[0:min(len(command), 35)],
    print "..." if len(command) > 35 else ""
    d.write(command)
    time.sleep(delay)


#w("*IDN?") # this appears to return an empty string(why?)
#d.read()

def set_arbitrary_waveform(channel, sample_rate, vpp, points, offset=0.0):
    c = channel
    DAC_BITS=16384

    # check data points
    for p in points:
        if (-vpp/2.0) > p or p > (vpp/2.0):
             print "unable to reconcile all points" 
             return
            

    w(":SOUR%d:APPL:ARB %.02f,%.04f,%.04f" % (c, sample_rate, vpp, offset))
    w(":SOUR%d:DATA:POINTS VOLATILE,%d" % (c, len(points)))
    for i,p in enumerate(points):
        value = int(p/(float(vpp)/DAC_BITS)) + (DAC_BITS / 2)
	w(":SOUR%d:DATA:VAL VOLATILE,%d,%d" % (c, i+1, value), 0.025)

def stair_step_generator(width, start, end, step, sample_rate=20):
    #sample_rate is in sa/S
    sample_width = int(sample_rate*width)
    points = []

    if (start < end) and step > 0:
	while start < end:
	    points += [start for x in range(sample_width)]
	    start += step
	points += [end for x in range(sample_width)]
    elif (end < start) and step < 0:
	while end < start:
	    points += [start for x in range(sample_width)]
	    start += step
	points += [end for x in range(sample_width)]
    else:
        sample_rate = 0

    return (sample_rate, points)
    
    
def plus_minus_116mv_pt06_test():
    w(":OUTP1 OFF")
    w(":OUTP2 OFF")
    #set_arbitrary_waveform(1, 1, 1.6384, [x/1000.0 for x in range(116,-116-8,-8)])
    sr, data = stair_step_generator(10, 116, -116, -8)
    data = [0 for x in range(sr*5)] + [p/1000.0 for p in data]
    set_arbitrary_waveform(1, sr, 1.6384, data)
    print len(data)
    w(":SOUR:TRACK INVERTED")
    w(":OUTP1 ON")
    w(":OUTP2 ON")
    # sync must be done after outputs enabled
    w(":SOUR1:PHAS:INIT")
    w(":SOUR2:PHAS:SYNC")


def shimmer_alone_test():
    w(":OUTP1 OFF")
    w(":OUTP2 OFF")
    sr, data = stair_step_generator(10, 1.18, 1.28, 0.0025)
    data = [0 for x in range(sr*5)] + data
    set_arbitrary_waveform(1, sr, 8.192, data, offset=1.23)
    print len(data)
    w(":OUTP1 ON")
    # sync must be done after outputs enabled
    w(":SOUR1:PHAS:INIT")

plus_minus_116mv_pt06_test()
#shimmer_alone_test()
